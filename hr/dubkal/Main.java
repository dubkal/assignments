package hr.dubkal;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        String assignment = "0";

        while (assignment.equals("0")) {
            System.out.println("\nPlease enter number of assignment(1, 2 or 3)" +
                    "\nType 4 for exit");

                assignment = scanner.nextLine();

            switch (assignment) {
                case "1":
                    Permutations.permutations();
                    assignment = "0";
                    break;

                case "2":
                    People.people();
                    assignment = "0";
                    break;

                case "3":
                    Brackets.brackets();
                    assignment = "0";
                    break;

                case "4": assignment = "4";
                    break;

                default: System.out.println("\nInvalid input");
                    assignment = "0";
                    break;
            }
        }


    }
}
