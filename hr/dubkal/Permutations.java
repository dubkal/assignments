package hr.dubkal;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

class Permutations {

     static void permutations() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter your string");
        String input = scanner.nextLine();
        Set<String> permutationsSet = Permutations.generatePermutations(input);
        for (String permutation : permutationsSet) {
            System.out.println(permutation);
        }
    }

    private static Set<String> generatePermutations(String input) {
        Set<String> resultSet = new HashSet<>();
        if (input.isEmpty()){
            return resultSet;
        }

        char a = input.charAt(0);
        if (input.length() > 1) {
            input = input.substring(1);
            Set<String> permutationsSet = generatePermutations(input);

            for (String x : permutationsSet) {
                for (int i = 0; i <= x.length(); i++) {
                    resultSet.add(x.substring(0, i) + a + x.substring(i));
                }
            }
        }
        else{
            resultSet.add(String.valueOf(a));
        }
        return resultSet;
    }
}
