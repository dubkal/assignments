package hr.dubkal;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

class Brackets {
     static void brackets() {

         Scanner scanner = new Scanner(System.in);
         System.out.println("Please enter string consisting only of []");

         String input = scanner.nextLine();

         if (!isValidInput(input)){
             System.out.println("There is something wrong with your string");
         }
         else {
             int index = findIndex(input);
             System.out.println(input + " - index is " + index);
         }
    }

    private static int findIndex(String input) {
         List<Integer> openList = new ArrayList<>();
         List<Integer> closedList = new ArrayList<>();
         int numberOfOpen = 0;
         int numberOfClosed = 0;
         int inputLength = input.length();

        for (int i = 0; i < inputLength; i++) {
            if (input.charAt(i) == '['){
                numberOfOpen += 1;
            }
            openList.add(numberOfOpen);
        }

        for (int i = inputLength - 1; i >= 0; i--) {
             if (input.charAt(i) == ']'){
                 numberOfClosed += 1;
             }
             closedList.add(0, numberOfClosed);
         }

        for (int i = inputLength - 1; i > 0; i--) {
            if (closedList.get(i) == openList.get(i - 1)){
                return i;
            }
        }

         return 0;
    }

    private static boolean isValidInput(String input){
         if (!(input.contains("[") && input.contains("]"))){
             return false;
         }
        for (char c: input.toCharArray()) {
            if (c != '[' && c !=']') return false;
        }

        return true;
    }
}
