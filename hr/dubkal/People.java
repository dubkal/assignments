package hr.dubkal;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

class People {
    static void people() {

        Pattern formatPattern = Pattern.compile("[a-zA-Z]+\\p{javaSpaceChar}[a-zA-Z]+");
        Scanner scanner = new Scanner(System.in);
        String input;
        List<Person> people = new ArrayList<>();
        System.out.println("Enter firstname and lastname separated by space\n" +
                "Type end to see lists");
        while (true){
            input = scanner.nextLine();
            if (input.equals("end")){
                break;
            }

            Matcher matcher = formatPattern.matcher(input);
            if (matcher.matches()){
                savePerson(input, people);
            }
            else System.out.println("Please enter correct format!");
        }
        printPeople(people);
    }

    private static void savePerson(String input, List<Person> people){
        Pattern personPattern = Pattern.compile("([a-zA-Z]+)\\p{javaSpaceChar}([a-zA-Z]+)");
        Matcher matcher = personPattern.matcher(input);
        if (matcher.find())
        {
            people.add(new Person(matcher.group(1) ,matcher.group(2)));
        }
    }

    private static void printPeople(List<Person> people) {

        List<Person> sortedByFirstName = people.stream()
                .sorted(Comparator.comparing(Person::getFirstName))
                .collect(Collectors.toList());

        List<Person> sortedByLastName = people.stream()
                .sorted(Comparator.comparing(Person::getLastName))
                .collect(Collectors.toList());

        List<Person> sortedByLastThenFirstName = people.stream()
                .sorted(Comparator.comparing(Person::getLastName).thenComparing(Person::getFirstName))
                .collect(Collectors.toList());

        System.out.println("\nSorted by firstname:");
        for (Person person : sortedByFirstName){
            System.out.println(person.toString());
        }
        System.out.println("\nSorted by lastname");
        for (Person person : sortedByLastName){
            System.out.println(person.toString());
        }
        System.out.println("\nSorted by lastname then firstname");
        for (Person person : sortedByLastThenFirstName){
            System.out.println(person.toString());
        }
    }
}
